import passport from "passport";
import {
  Strategy as JwtStrategy,
  ExtractJwt,
  JwtFromRequestFunction,
} from "passport-jwt";
import jwt from "jsonwebtoken";
//@ts-expect-error
import permission from "permission";
import * as core from "express-serve-static-core";
import { Request, RequestHandler, Response, NextFunction } from "express";

import {
  isAuthenticatedRequest,
  isCustomValidator,
  isSelfObject,
  withModelAndMethod,
} from "./validators";

import { validateOwnership } from "./ownership";
import {
  BaseUser,
  Configuration,
  CustomValidator,
  RuleKey,
  Rules,
  Self,
} from "../types";

class Authenticator<User extends BaseUser> {
  _opts: { [key in string]: any } = {};

  _getExtractors(extractors?: string | JwtFromRequestFunction[]) {
    let methods: JwtFromRequestFunction[] = [];
    if (Array.isArray(extractors)) {
      methods = extractors;
    } else if (typeof extractors === "string") {
      switch (extractors) {
        case "only-header":
          methods.push(ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
          break;
        case "url-header":
          methods.push(ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
          methods.push(ExtractJwt.fromUrlQueryParameter("auth_token"));
          break;
        case "only-url":
          methods.push(ExtractJwt.fromUrlQueryParameter("auth_token"));
          break;
        default:
          methods.push(ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
          break;
      }
    } else {
      methods.push(ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
    }
    return ExtractJwt.fromExtractors(methods);
  }

  /**
   * Configure passport strategy
   *
   * @param {string} secret the jwt secret string
   * @param {function} authenticate a method that the received the token sub and must return the user if its valid
   * @param {string} strategyname [optional] [default: jwt]
   * @param {string|array|null} extractors [optional] jwt extractors, can be string [only-header, url-header, only-url] or array with ExtractJwt instances [default: only-header]
   */
  configure(
    secret: string,
    authenticate: (sub: any) => Promise<User>,
    strategyname: string = "jwt",
    extractors: string | JwtFromRequestFunction[] = "only-header"
  ) {
    this._configure(secret, authenticate, strategyname, extractors);
    return passport.initialize();
  }

  /**
   * Configure passport strategies
   *
   * @see configure for more info
   */
  configureFromArray(configurations: Configuration<User>[]) {
    configurations.forEach((config) =>
      this._configure(
        config.secret,
        config.authenticate,
        config.strategyname,
        config.extractors
      )
    );
    return passport.initialize();
  }

  /**
   * Validates that a jwt is received and uses the authenticate method to validate the logged user
   */
  _configure(
    secret: string,
    getUser: (sub: any) => Promise<User>,
    strategyname: string = "jwt",
    extractors: string | JwtFromRequestFunction[] = "only-header"
  ) {
    if (!this._opts[strategyname]) {
      this._opts[strategyname] = {
        authScheme: "Bearer",
      };
    }
    this._opts[strategyname].secretOrKey = secret;
    this._opts[strategyname].jwtFromRequest = this._getExtractors(extractors);
    passport.use(
      strategyname,
      new JwtStrategy(this._opts[strategyname], (jwt_payload, done) => {
        getUser(jwt_payload.sub)
          .then((usr) => done(null, usr))
          .catch((err) => {
            console.error(
              `\x1b[30m[Smaug]\x1b[0m \x1b[31m Unexpected error\x1b[0m: `,
              err
            );
            return done(err);
          });
      })
    );
  }

  /**
   * generate jwt hash (jwt.io). Expires uses ms syntax (https://github.com/rauchg/ms.js)
   * @param {object} data
   * @param {string} expires
   * @returns {string} token
   */
  getToken<T extends { [key in string]: any }>(
    data: T,
    expires: string,
    strategy = "jwt"
  ): string {
    return jwt.sign(data, this._opts[strategy].secretOrKey, {
      expiresIn: expires,
    });
  }

  /**
   * Check user login and permissions
   * @param {array} roles
   * @returns {array of functions}
   */
  authenticate<
    P = core.ParamsDictionary,
    ResBody = any,
    ReqBody = any,
    ReqQuery = qs.ParsedQs
  >(
    roles?: string | string[],
    strategy: string = "jwt"
  ): (RequestHandler<P, ResBody, ReqBody, ReqQuery> & { user: User })[] {
    const config = { session: false };
    if (!roles || roles == "*") {
      return [passport.authenticate(strategy, config)];
    }
    return [passport.authenticate(strategy, config), permission(roles)];
  }

  /**
   * Check user login and permissions with user rules
   */
  authorize<REQ extends Request, RES extends Response>(
    req: REQ,
    res: RES,
    next: NextFunction,
    model: keyof Rules,
    method: RuleKey,
    custom?: CustomValidator | Self,
    strategy = "jwt"
  ) {
    // Check if it authorized
    passport.authenticate(strategy, {
      session: false,
    })(req, res, () => {
      if (!isAuthenticatedRequest(req)) {
        return res.status(403).send("notAuthorized");
      }
      // Check if the user has the rules to access this method
      if (withModelAndMethod(req, model, method)) {
        // The user has full access to this method
        if (req.user.Rules[model]![method] === true) {
          return next();
        }
        // Check if the user is validating with a custom method
        if (
          typeof req.user.Rules[model]![method] === "string" &&
          req.user.Rules[model]![method] === "custom" &&
          isCustomValidator(custom)
        ) {
          return validate(custom(req), res, next);
        }
        // Check if the user needs to be owner of the resource
        if (
          typeof req.user.Rules[model]![method] === "string" &&
          req.user.Rules[model]![method] === "self" &&
          isSelfObject(custom)
        ) {
          return validate(validateOwnership(req, custom), res, next);
        }
      }
      // The user is not authorized
      return res.status(403).send("notAuthorized");
    });
  }
}

export default new Authenticator();

async function validate(
  promise: Promise<boolean>,
  res: Response,
  next: NextFunction
) {
  try {
    const success = await promise;
    if (success) {
      return next();
    }
    throw new Error("validation failed");
  } catch (error) {
    console.error(
      `\x1b[30m[Smaug]\x1b[0m \x1b[31m Unexpected error\x1b[0m: `,
      error
    );
    return res.status(403).send("notAuthorized");
  }
}
