import { Request } from "express";
import { AuthenticatedRequest, Self } from "../types";

/*
If parameters are received by URL, the developer may decide to change 
the name of the parameter received. Therefore, the case can be given 
where the same property is received with different values.

For example:
------------
Endpoint: https://domain/endpoint/:id
https://domain/endpoint/asd WITH BODY: {_id: 'qwe'}

If self = {param: 'id, db: '_id'} hasDifferentKeys will return false
*/
export function validateDuplicate(
  request: AuthenticatedRequest,
  self: Self,
  requestData: any = {}
) {
  const paramKey = self.param;
  const dbKey = self.db?.modelKey;
  const param = paramKey
    ? request.params[paramKey as keyof typeof request.params]
    : false;
  const data = {
    db: dbKey ? requestData[dbKey] || false : false,
    param: paramKey ? requestData[paramKey] || false : false,
  };
  if (
    param &&
    ((data.db && data.db !== param) || (data.param && data.param !== param))
  ) {
    return false;
  }
  return true;
}

/**
 * Checks if the same property is present in different objects (as "body" or "query") and joins them with "params" in an unique structure
 */
export function joinParameters(req: Request) {
  // Get the method where the params are
  let request: {
    [key in string]: any;
  } = {};
  // Get parameters
  switch (req.method) {
    case "POST":
    case "PATCH":
      request = req.body;
      break;
    case "GET":
      request = req.query;
      if (request.query) {
        let query = {};
        if (typeof request.query == "string") {
          try {
            query = JSON.parse(request.query);
          } catch (e) {
            query = {};
          }
        }
        // request keys will be overridden by query keys
        Object.assign(request, query);
      }
      break;
  }
  // Method keys will be overridden by params keys
  Object.assign(request, req.params || {});
  return request;
}
