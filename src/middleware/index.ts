import express, { RequestHandler } from "express";

import kAuth from "../Authenticator";

import { getAuth, getMiddlewares, getWithParameterMiddleware } from "./helpers";
import { isWithAuthenticationUserProps } from "./validators";
import {
  AuthMiddleware,
  AuthenticatedUserBy,
  CustomValidator,
  RuleKey,
  Self,
  WithAuthenticatedKey,
  WithAuthenticatedUserProps,
  WithID,
  WithUsername,
} from "../../types";

/**
 * Adds an express middleware that will validate if there is a logged user
 *
 * @param middleware [optional] The endpoint method that will be called if all middleware succeeded
 */
export function withAuthentication<
  MiddlewareType extends AuthMiddleware = AuthMiddleware
>(middleware?: MiddlewareType | MiddlewareType[]): RequestHandler[] {
  const auth = kAuth.authenticate("*");
  return getMiddlewares(auth, middleware);
}

/**
 * Adds an express middleware that will validate if the logged user type is listed in the received roles
 *
 * @param roles The roles that will use the khem-auth library
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export function withAuthorization<
  MiddlewareType extends AuthMiddleware = AuthMiddleware
>(
  roles: string | string[],
  middleware?: MiddlewareType | MiddlewareType[]
): RequestHandler[] {
  const auth = kAuth.authenticate(roles);
  return getMiddlewares(auth, middleware);
}

/**
 * Adds an express middleware that will validate if the logged user complies with the rules saved in the user object
 *
 * @param model The name of the model for the authorization role
 * @param method the name of the method that will access "list" | "read" | "create" | "update" | "delete"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export function withRulesAuthorization<
  MiddlewareType extends AuthMiddleware = AuthMiddleware
>(
  model: string,
  method: RuleKey,
  middleware?: MiddlewareType | MiddlewareType[],
  custom?: CustomValidator | Self
): RequestHandler[] {
  const auth: RequestHandler = (req, res, next) => {
    return kAuth.authorize(req, res, next, model, method, custom);
  };
  return getMiddlewares(auth, middleware);
}

/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "id" is the same as the logged user
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of authentication middlewares (khem-auth) and use them to validate the request
 *
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user type is needed
 *
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export function withAuthenticatedUserId(
  props: WithAuthenticatedKey<WithID>
): RequestHandler[] {
  return withAuthenticatedUser<"id", AuthenticatedUserBy<"id">>({
    paramKey: "id",
    ...props,
  });
}

/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "username" is the same as the logged user
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of authentication middlewares (khem-auth) and use them to validate the request
 *
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user types is needed
 *
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export function withAuthenticatedUsername(
  props: WithAuthenticatedKey<WithUsername>
): RequestHandler[] {
  return withAuthenticatedUser<"username", AuthenticatedUserBy<"username">>({
    paramKey: "username",
    ...props,
  });
}

/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "paramKey" is the same as the logged user _id
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of khem-auth authentication middlewares and use them to validate the request
 *
 * @param paramKey The parameter key used to validate the logged user
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user types is needed
 *
 * @param paramKey The parameter key used to validate the logged user
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export function withAuthenticatedUser<
  ParamKey extends string,
  MiddlewareType extends AuthenticatedUserBy<ParamKey>
>(
  props: WithAuthenticatedUserProps<ParamKey, MiddlewareType>
): RequestHandler[] {
  if (isWithAuthenticationUserProps(props)) {
    return getMiddlewares(getAuth(props.authentication), props.middleware);
  }

  return getMiddlewares(
    [
      ...withAuthentication(),
      getWithParameterMiddleware(props.paramKey || "id", props.adminTypes),
    ],
    props.middleware
  );
}

/**
 * Don't use this middleware unless absolutely necessary
 * -----------------------------------------------------
 *
 * Passing a JWT in the URL query is a bad practice as defined in the protocol
 *
 * @see https://tools.ietf.org/html/rfc6750
 */
export function UNSAFE_setAuthorizationHeaderFromQueryToken() {
  return (
    req: express.Request,
    _: express.Response,
    next: express.NextFunction
  ) => {
    // @ts-ignore
    req.headers.authorization = `Bearer ${req.query.token}`;
    next();
  };
}
