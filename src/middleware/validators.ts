import {
  AuthenticatedUserBy,
  WithAuthenticatedUserProps,
  WithAuthenticationUserProps,
} from "../../types";

export function isWithAuthenticationUserProps<
  ParamKey extends string,
  MiddlewareType extends AuthenticatedUserBy<ParamKey>
>(
  props: WithAuthenticatedUserProps<ParamKey, MiddlewareType>
): props is WithAuthenticationUserProps<ParamKey, MiddlewareType> {
  return props.hasOwnProperty("authentication");
}
