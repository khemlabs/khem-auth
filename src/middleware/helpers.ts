import express, { RequestHandler } from "express";

import { HttpError, HttpErrorCode } from "@khemlabs/error";
import {
  AuthenticatedRequest,
  RequestWithId,
  ValidUsernameRequest,
} from "../../types";

/**
 * Creates and return the middleware that will validate the received parameter
 *
 * @param param The parameter key received in the request
 */
export function getWithParameterMiddleware(
  param: string,
  adminTypes: string[] = []
) {
  const validateRequest = (
    req: any,
    _res: express.Response,
    next: express.NextFunction
  ) => {
    if (!req.user) {
      return next(
        new HttpError(HttpErrorCode.UNAUTHORIZED, "user is not logged")
      );
    }

    if (adminTypes.length > 0 && adminTypes.includes(req.user.type)) {
      return next();
    }

    // Invalid parameters
    if (!validUserKeyInRequest(param, req)) {
      return next(
        new HttpError(HttpErrorCode.BAD_REQUEST, "invalid parameters")
      );
    }

    const { params, user } = req;
    // Parameter not found or differs from logged user
    if (
      !(user as any)[param] ||
      (params as any)[param] !== (user as any)[param]
    ) {
      return next(
        new HttpError(
          HttpErrorCode.UNAUTHORIZED,
          `user ${req.user.username} can't access (${
            req.params.username || ""
          })`
        )
      );
    }

    // Continue
    return next();
  };

  return validateRequest;
}

export function validUserKeyInRequest(
  param: string,
  req: AuthenticatedRequest<any>
): req is ValidUsernameRequest {
  return req.params && typeof req.params[param] === "string";
}

export function getMiddlewares(
  authMiddlewares: RequestHandler | RequestHandler[],
  middleware?: any
): any[] {
  return middleware
    ? [...getAuth(authMiddlewares), ...getAuth(middleware)]
    : [...getAuth(authMiddlewares)];
}

export function getAuth(
  authentication: RequestHandler | RequestHandler[]
): RequestHandler[] {
  return Array.isArray(authentication) ? authentication : [authentication];
}

export function isRequestWithId(
  req: any
): req is AuthenticatedRequest<RequestWithId> {
  return !!req && !!req.params && !!req.params.id;
}
