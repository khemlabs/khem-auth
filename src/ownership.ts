import { AuthenticatedRequest, Self } from "../types";
import { joinParameters, validateDuplicate } from "./helpers";

/**
 * This method validates that the key received is the id of the logged user and if a mongoose model
 * is received that the resource is owned by the received id
 */
export async function validateOwnership(
  request: AuthenticatedRequest,
  self: Self
) {
  const params = joinParameters(request);
  // Check that the user has the parameter required to validate:
  if (params[self.param]) {
    // Validate repeated keys with different values
    if (!validateDuplicate(request, self, params)) {
      throw new Error(
        "duplicated keys found on request, do not send the same key in body, query or params"
      );
    }

    // If a mongoose model was received use it to search in database,
    // if not it will assume that is the user id
    if (request.user._id !== params[self.param]) {
      throw new Error(
        `the logged user (${request.user._id}) differs from the request (${
          self.param
        }=${params[self.param] || "unknown"})`
      );
    }

    // Check if we need to validate if the logged user is the owner of the resource
    const validated =
      !self.db ||
      !!(await self.db.model.findOne({
        [self.db.modelKey]: params[self.param],
      }));

    if (validated) {
      return true;
    }

    throw new Error(
      `the user with ${self.param} (${
        params[self.param]
      } || 'unknown') was not found or is not logged`
    );
  }

  // Self is not present on the logged user method rules object but is required
  throw new Error(
    `the the key (${self.param}) was not present in body, query or params`
  );
}
