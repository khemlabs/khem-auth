import { Request } from "express";
import { AuthenticatedRequest, CustomValidator, RuleKey, Self } from "../types";

/**
 * Checks that the model is present in the user object and that the method key is present in the model key
 */
export function withModelAndMethod<MO extends string, ME extends RuleKey>(
  req: AuthenticatedRequest,
  model: MO,
  method: ME
): boolean {
  return (
    req.user &&
    req.user.Rules &&
    !!req.user.Rules[model] &&
    !!req.user.Rules[model]![method]
  );
}

/**
 * Validates if the received object has a valid self object
 */
export function isSelfObject(self: any): self is Self {
  return (
    !!self &&
    typeof self === "object" &&
    typeof self.param === "string" &&
    (!self.mongo ||
      (typeof self.mongo === "object" &&
        typeof self.mongo.modelKey === "string" &&
        typeof self.mongo.model === "object" &&
        typeof self.mongo.model.findOne === "function"))
  );
}

export function isCustomValidator(
  validator: any
): validator is CustomValidator {
  return !!validator && typeof validator === "function";
}

export function isAuthenticatedRequest(
  req: Request
): req is AuthenticatedRequest {
  return !!req && !!(req as any).user && !!(req as any).user.username;
}
