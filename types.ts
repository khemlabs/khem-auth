import type { NextFunction, Request, Response, RequestHandler } from "express";

import authenticator from "./src/Authenticator";
import { JwtFromRequestFunction } from "passport-jwt";

export type UserValidator = boolean | "custom" | "self";

export type UserValidation = {
  [key in RuleKey]?: UserValidator;
};

export type UserRules = {
  [key in string]?: UserValidation;
};

export type BaseUser = {
  _id: string;
  username: string;
  Rules: UserRules;
  type: string;
};

export type ValidatorParams = {
  filters: Rules;
  request: AuthenticatedRequest;
};

export type CustomValidator = (req: AuthenticatedRequest) => Promise<boolean>;

export type Self = {
  param: string;
  db?: {
    modelKey: string;
    model: {
      findOne: (search: { [key in string]: string }) => Promise<
        object | null | undefined
      >;
    };
  };
};

export type Validator = boolean | "custom" | Self;

export type RuleKey = "list" | "read" | "create" | "update" | "delete";

export type Validation = {
  [key in RuleKey]?: Validator;
};

export type Rules = {
  [key in string]?: Validation;
};

export type ExpressParamsType = { [key: string]: string };

export interface AuthenticatedRequest<
  PARAMS extends ExpressParamsType = {},
  BODY extends any = {},
  USER extends BaseUser = BaseUser
> extends Request<PARAMS, {}, BODY> {
  user: USER;
}

// MIDDLEWARE TYPES

export type MiddleWare<
  Req extends Request = Request,
  Res extends Response = Response
> = (req: Req, res: Res, next: NextFunction) => any;

export type AuthMiddleware<
  ReqType extends AuthenticatedRequest = AuthenticatedRequest,
  ResType extends Response = Response
> = MiddleWare<ReqType, ResType>;

export type WithAuthenticatedKey<MiddlewareType = WithID | WithUsername> =
  | WithAuthenticationKey<MiddlewareType>
  | WithCustomValidatorKey<MiddlewareType>;

export type WithAuthenticationKey<MiddlewareType = WithID | WithUsername> = {
  authentication: RequestHandler | RequestHandler[];
  middleware?: MiddlewareType;
};

export type WithAuthenticatedUserProps<
  ParamKey extends string,
  MiddlewareType extends AuthenticatedUserBy<ParamKey>
> =
  | WithAuthenticationUserProps<ParamKey, MiddlewareType>
  | WithCustomValidatorUserProps<ParamKey, MiddlewareType>;

export type WithCustomValidatorUserProps<
  ParamKey extends string,
  MiddlewareType extends AuthenticatedUserBy<ParamKey>
> = {
  paramKey: ParamKey;
  adminTypes: string[];
  middleware?:
    | AuthUserMiddleware<MiddlewareType>
    | AuthUserMiddleware<MiddlewareType>[];
};

export type WithAuthenticationUserProps<
  ParamKey extends string,
  MiddlewareType extends AuthenticatedUserBy<ParamKey>
> = {
  paramKey: ParamKey;
  authentication: RequestHandler | RequestHandler[];
  middleware?:
    | AuthUserMiddleware<MiddlewareType>
    | AuthUserMiddleware<MiddlewareType>[];
};

export type WithCustomValidatorKey<MiddlewareType = WithID | WithUsername> = {
  adminTypes: string[];
  middleware?: MiddlewareType;
};

export type WithID = AuthUserMiddleware<AuthenticatedUserBy<"id">>;

export type WithUsername = AuthUserMiddleware<AuthenticatedUserBy<"username">>;

export type WithIdParams = {
  id: string;
};

export type WithUsernameParams = {
  username: string;
};

export type AuthenticatedUserBy<KEY extends string> = AuthenticatedRequest<{
  [key in KEY]: string;
}>;

export type RequestWithId = { id: string };

export type AuthUserMiddleware<
  Req extends AuthenticatedRequest = AuthenticatedRequest,
  Res extends Response = Response
> = MiddleWare<Req, Res>;

export type ValidUsernameRequest = AuthenticatedRequest<WithUsernameParams>;

export type Authenticator = typeof authenticator;

export type ValidIdRequest = AuthenticatedRequest<WithIdParams>;

export type Configuration<T extends BaseUser> = {
  secret: string;
  authenticate: (sub: any) => Promise<T>;
  strategyname?: string;
  extractors?: string | JwtFromRequestFunction[];
};
