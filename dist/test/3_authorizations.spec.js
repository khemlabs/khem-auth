"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var chai_1 = __importDefault(require("chai"));
var Authenticator_1 = __importDefault(require("../src/Authenticator"));
var tokens_1 = require("./tokens");
var middleware_1 = require("./middleware");
var data_1 = require("./data");
var generators_1 = require("./generators");
describe("Validate authorizations: ", function () {
    it("it expects to authorize create request (admin user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("adminToken"), "private", "create");
    });
    it("it expects to unauthorize create request (list user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("listToken"), "private", "create", undefined, undefined, 403);
    });
    it("it expects to authorize update request (update user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("updateToken"), "private", "update");
    });
    it("it expects to unauthorize update request (self user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("listToken"), "private", "update", undefined, undefined, 403);
    });
    it("it expects to authorize list request (list user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("listToken"), "private", "list");
    });
    it("it expects to unauthorize list request (self user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("selfToken"), "private", "list", undefined, undefined, 403);
    });
    it("it expects to authorize list request (self user) ", function (done) {
        authorize(done, (0, tokens_1.getToken)("selfToken"), "private", "list", {
            param: "user",
        }, { user: data_1.selfUser._id });
    });
    it("it expects to authorize update request (self user with db) ", function (done) {
        var _this = this;
        authorize(done, (0, tokens_1.getToken)("selfToken"), "private", "list", {
            param: "user",
            db: {
                model: {
                    findOne: function (where) { return __awaiter(_this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            if (where.customKey === data_1.selfUser._id) {
                                return [2 /*return*/, data_1.selfUser];
                            }
                            return [2 /*return*/, null];
                        });
                    }); },
                },
                modelKey: "customKey",
            },
        }, { user: data_1.selfUser._id });
    });
    it("it expects to authorize list request (custom user) ", function (done) {
        var _this = this;
        authorize(done, (0, tokens_1.getToken)("customToken"), "private", "list", function (req) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, req.user.username === data_1.customUser.username];
            });
        }); });
    });
});
function authorize(done, token, model, method, custom, params, expectCode) {
    var middleware = (0, middleware_1.getMiddleware)();
    chai_1.default.expect(middleware).to.be.a("function");
    chai_1.default.expect(token).to.be.a("string");
    chai_1.default.expect(token.length).to.be.greaterThan(0);
    var request = (0, generators_1.getRequest)(token, params);
    var response = (0, generators_1.getUnauthorizedResponse)(done, expectCode);
    middleware(request, response, function () {
        var next = (function (_req) {
            done();
        });
        Authenticator_1.default.authorize(request, response, next, model, method, custom);
    });
}
//# sourceMappingURL=3_authorizations.spec.js.map