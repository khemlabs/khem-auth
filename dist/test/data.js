"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.users = exports.customUser = exports.selfUser = exports.listUser = exports.updateUser = exports.adminUser = exports.custom = exports.self = exports.list = exports.update = exports.full = exports.secret = void 0;
exports.secret = "123";
exports.full = {
    list: true,
    read: true,
    update: true,
    create: true,
    delete: true,
};
exports.update = {
    list: true,
    read: true,
    update: true,
    create: true,
    delete: false,
};
exports.list = {
    list: true,
    read: true,
    update: false,
    create: false,
    delete: false,
};
exports.self = {
    list: "self",
    read: "self",
    update: false,
    create: false,
    delete: false,
};
exports.custom = {
    list: "custom",
    read: "custom",
    update: false,
    create: false,
    delete: false,
};
exports.adminUser = {
    _id: "1",
    username: "admin",
    type: "admin",
    Rules: {
        user: exports.full,
        private: exports.full,
    },
};
exports.updateUser = {
    _id: "1",
    username: "admin",
    type: "admin",
    Rules: {
        user: exports.update,
        private: exports.update,
    },
};
exports.listUser = {
    _id: "2",
    username: "list",
    type: "list",
    Rules: {
        user: exports.list,
        private: exports.list,
    },
};
exports.selfUser = {
    _id: "3",
    username: "self",
    type: "self",
    Rules: {
        user: exports.self,
        private: exports.self,
    },
};
exports.customUser = {
    _id: "4",
    username: "custom",
    type: "custom",
    Rules: {
        user: exports.custom,
        private: exports.custom,
    },
};
exports.users = [
    exports.adminUser,
    exports.updateUser,
    exports.listUser,
    exports.selfUser,
    exports.customUser,
];
//# sourceMappingURL=data.js.map