"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMiddleware = exports.setMiddleWare = void 0;
var middleware = null;
function setMiddleWare(method) {
    middleware = method;
}
exports.setMiddleWare = setMiddleWare;
function getMiddleware() {
    return middleware;
}
exports.getMiddleware = getMiddleware;
//# sourceMappingURL=middleware.js.map