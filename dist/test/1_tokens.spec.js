"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var chai_1 = __importDefault(require("chai"));
var Authenticator_1 = __importDefault(require("../src/Authenticator"));
var data_1 = require("./data");
var tokens_1 = require("./tokens");
var middleware_1 = require("./middleware");
describe("Validate token generation: ", function () {
    it("kAuth should be configured", function (done) {
        (0, middleware_1.setMiddleWare)(Authenticator_1.default.configure(data_1.secret, searchUser));
        var middleware = (0, middleware_1.getMiddleware)();
        chai_1.default.expect(middleware).to.be.a("function");
        done();
    });
    it("it expects to generate an admin token", function (done) {
        (0, tokens_1.setToken)("adminToken", Authenticator_1.default.getToken({ sub: data_1.adminUser }, "10m"));
        var adminToken = (0, tokens_1.getToken)("adminToken");
        chai_1.default.expect(adminToken).to.be.a("string");
        chai_1.default.expect(adminToken.length).to.be.greaterThan(0);
        done();
    });
    it("it expects to generate an update token", function (done) {
        (0, tokens_1.setToken)("updateToken", Authenticator_1.default.getToken({ sub: data_1.updateUser }, "10m"));
        var updateToken = (0, tokens_1.getToken)("updateToken");
        chai_1.default.expect(updateToken).to.be.a("string");
        chai_1.default.expect(updateToken.length).to.be.greaterThan(0);
        done();
    });
    it("it expects to generate a list token", function (done) {
        (0, tokens_1.setToken)("listToken", Authenticator_1.default.getToken({ sub: data_1.listUser }, "10m"));
        var listToken = (0, tokens_1.getToken)("listToken");
        chai_1.default.expect(listToken).to.be.a("string");
        chai_1.default.expect(listToken.length).to.be.greaterThan(0);
        done();
    });
    it("it expects to generate a self token", function (done) {
        (0, tokens_1.setToken)("selfToken", Authenticator_1.default.getToken({ sub: data_1.selfUser }, "10m"));
        var selfToken = (0, tokens_1.getToken)("selfToken");
        chai_1.default.expect(selfToken).to.be.a("string");
        chai_1.default.expect(selfToken.length).to.be.greaterThan(0);
        done();
    });
    it("it expects to generate a custom token", function (done) {
        (0, tokens_1.setToken)("customToken", Authenticator_1.default.getToken({ sub: data_1.customUser }, "10m"));
        var customToken = (0, tokens_1.getToken)("customToken");
        chai_1.default.expect(customToken).to.be.a("string");
        chai_1.default.expect(customToken.length).to.be.greaterThan(0);
        done();
    });
});
function searchUser(data) {
    return __awaiter(this, void 0, void 0, function () {
        var found;
        return __generator(this, function (_a) {
            found = !!data._id && data_1.users.find(function (user) { return user._id === data._id; });
            if (!found) {
                throw new Error("user not found");
            }
            return [2 /*return*/, found];
        });
    });
}
//# sourceMappingURL=1_tokens.spec.js.map