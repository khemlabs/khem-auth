"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUnauthorizedResponse = exports.getRequest = void 0;
var node_mocks_http_1 = __importDefault(require("node-mocks-http"));
var chai_1 = __importDefault(require("chai"));
function getRequest(token, params) {
    var request = node_mocks_http_1.default.createRequest({
        headers: {
            authorization: "Bearer ".concat(token),
        },
        params: params,
    });
    return request;
}
exports.getRequest = getRequest;
function getUnauthorizedResponse(done, expectCode) {
    var doneCalled = false;
    // @TODO: NOT WORKING
    // const response = httpMocks.createResponse();
    // response.on("finish", () => {
    //   try {
    //     console.debug(response.statusCode);
    //     if (expectCode) {
    //       chai.expect(response.statusCode).to.equal(expectCode);
    //     }
    //     done();
    //   } catch (error) {
    //     done(error);
    //   }
    // });
    // response.on("error", (error) => {
    //   console.debug("error: ", error);
    //   done(error);
    // });
    var response = {
        status: function (code) {
            try {
                chai_1.default
                    .expect(expectCode, "unexpected code (".concat(code, ") was received"))
                    .to.be.a("number");
                chai_1.default.expect(expectCode).to.equal(code);
                if (!doneCalled) {
                    doneCalled = true;
                    done();
                }
            }
            catch (error) {
                if (!doneCalled) {
                    doneCalled = true;
                    done(error);
                }
            }
            return {
                send: function () {
                    try {
                        chai_1.default
                            .expect(expectCode, "unexpected code (".concat(code, ") was received"))
                            .to.be.a("number");
                        chai_1.default.expect(expectCode).to.equal(code);
                        if (!doneCalled) {
                            doneCalled = true;
                            done();
                        }
                    }
                    catch (error) {
                        if (!doneCalled) {
                            doneCalled = true;
                            done(error);
                        }
                    }
                },
            };
        },
        end: function (data) {
            try {
                chai_1.default
                    .expect(expectCode, "unexpected response (".concat(data, ") was received"))
                    .to.be.a("number");
                chai_1.default.expect(expectCode).to.equal(401);
                chai_1.default.expect(data).to.equal("Unauthorized");
                if (!doneCalled) {
                    doneCalled = true;
                    done();
                }
            }
            catch (error) {
                if (!doneCalled) {
                    doneCalled = true;
                    done(error);
                }
            }
        },
        send: function (data) {
            try {
                chai_1.default
                    .expect(expectCode, "unexpected response (".concat(data, ") was received"))
                    .to.be.a("number");
                chai_1.default.expect(expectCode).to.equal(401);
                chai_1.default.expect(data).to.equal("Unauthorized");
                if (!doneCalled) {
                    doneCalled = true;
                    done();
                }
            }
            catch (error) {
                if (!doneCalled) {
                    doneCalled = true;
                    done(error);
                }
            }
        },
    };
    return response;
}
exports.getUnauthorizedResponse = getUnauthorizedResponse;
//# sourceMappingURL=generators.js.map