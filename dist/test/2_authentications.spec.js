"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var chai_1 = __importDefault(require("chai"));
var Authenticator_1 = __importDefault(require("../src/Authenticator"));
var tokens_1 = require("./tokens");
var middleware_1 = require("./middleware");
var generators_1 = require("./generators");
describe("Validate authentications: ", function () {
    it("it expects to authorize request (admin user) ", function (done) {
        authenticate(done, (0, tokens_1.getToken)("adminToken"));
    });
    it("it expects to authorize request (update user) ", function (done) {
        authenticate(done, (0, tokens_1.getToken)("updateToken"));
    });
    it("it expects to authorize request (list user) ", function (done) {
        authenticate(done, (0, tokens_1.getToken)("listToken"));
    });
    it("it expects to authorize request (self user) ", function (done) {
        authenticate(done, (0, tokens_1.getToken)("selfToken"));
    });
    it("it expects to authorize request (custom user) ", function (done) {
        authenticate(done, (0, tokens_1.getToken)("customToken"));
    });
    it("it expects to unauthorize request (without user) ", function (done) {
        authenticate(done, "JsdafkdnjsvINVALIDTOKEN", 401);
    });
});
function authenticate(done, token, expectCode) {
    var middleware = (0, middleware_1.getMiddleware)();
    chai_1.default.expect(middleware).to.be.a("function");
    chai_1.default.expect(token).to.be.a("string");
    chai_1.default.expect(token.length).to.be.greaterThan(0);
    var request = (0, generators_1.getRequest)(token);
    var response = (0, generators_1.getUnauthorizedResponse)(done, expectCode);
    middleware(request, response, function () {
        var all = Authenticator_1.default.authenticate("*");
        all.every(function (auth, index) {
            return auth(request, response, function () {
                if (index === all.length - 1) {
                    chai_1.default.expect(request.user).to.be.an("object");
                    chai_1.default.expect(request.user._id).to.be.a("string");
                    done();
                }
            });
        });
    });
}
//# sourceMappingURL=2_authentications.spec.js.map