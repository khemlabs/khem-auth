"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.setToken = exports.getToken = void 0;
var tokens = {
    adminToken: "",
    updateToken: "",
    listToken: "",
    selfToken: "",
    customToken: "",
};
function getToken(key) {
    return tokens[key];
}
exports.getToken = getToken;
function setToken(key, value) {
    tokens[key] = value;
}
exports.setToken = setToken;
//# sourceMappingURL=tokens.js.map