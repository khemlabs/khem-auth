declare const tokens: {
    adminToken: string;
    updateToken: string;
    listToken: string;
    selfToken: string;
    customToken: string;
};
export declare function getToken(key: keyof typeof tokens): string;
export declare function setToken(key: keyof typeof tokens, value: string): void;
export {};
