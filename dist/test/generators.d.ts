/// <reference types="qs" />
/// <reference types="express" />
/// <reference types="mocha" />
import httpMocks from "node-mocks-http";
export declare function getRequest(token: string, params?: {
    [key in string]: any;
}): httpMocks.MockRequest<import("express").Request<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs, Record<string, any>>>;
export declare function getUnauthorizedResponse(done: Mocha.Done, expectCode?: number): any;
