"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var passport_1 = __importDefault(require("passport"));
var passport_jwt_1 = require("passport-jwt");
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
//@ts-expect-error
var permission_1 = __importDefault(require("permission"));
var validators_1 = require("./validators");
var ownership_1 = require("./ownership");
var Authenticator = /** @class */ (function () {
    function Authenticator() {
        this._opts = {};
    }
    Authenticator.prototype._getExtractors = function (extractors) {
        var methods = [];
        if (Array.isArray(extractors)) {
            methods = extractors;
        }
        else if (typeof extractors === "string") {
            switch (extractors) {
                case "only-header":
                    methods.push(passport_jwt_1.ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
                    break;
                case "url-header":
                    methods.push(passport_jwt_1.ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
                    methods.push(passport_jwt_1.ExtractJwt.fromUrlQueryParameter("auth_token"));
                    break;
                case "only-url":
                    methods.push(passport_jwt_1.ExtractJwt.fromUrlQueryParameter("auth_token"));
                    break;
                default:
                    methods.push(passport_jwt_1.ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
                    break;
            }
        }
        else {
            methods.push(passport_jwt_1.ExtractJwt.fromAuthHeaderWithScheme("Bearer"));
        }
        return passport_jwt_1.ExtractJwt.fromExtractors(methods);
    };
    /**
     * Configure passport strategy
     *
     * @param {string} secret the jwt secret string
     * @param {function} authenticate a method that the received the token sub and must return the user if its valid
     * @param {string} strategyname [optional] [default: jwt]
     * @param {string|array|null} extractors [optional] jwt extractors, can be string [only-header, url-header, only-url] or array with ExtractJwt instances [default: only-header]
     */
    Authenticator.prototype.configure = function (secret, authenticate, strategyname, extractors) {
        if (strategyname === void 0) { strategyname = "jwt"; }
        if (extractors === void 0) { extractors = "only-header"; }
        this._configure(secret, authenticate, strategyname, extractors);
        return passport_1.default.initialize();
    };
    /**
     * Configure passport strategies
     *
     * @see configure for more info
     */
    Authenticator.prototype.configureFromArray = function (configurations) {
        var _this = this;
        configurations.forEach(function (config) {
            return _this._configure(config.secret, config.authenticate, config.strategyname, config.extractors);
        });
        return passport_1.default.initialize();
    };
    /**
     * Validates that a jwt is received and uses the authenticate method to validate the logged user
     */
    Authenticator.prototype._configure = function (secret, getUser, strategyname, extractors) {
        if (strategyname === void 0) { strategyname = "jwt"; }
        if (extractors === void 0) { extractors = "only-header"; }
        if (!this._opts[strategyname]) {
            this._opts[strategyname] = {
                authScheme: "Bearer",
            };
        }
        this._opts[strategyname].secretOrKey = secret;
        this._opts[strategyname].jwtFromRequest = this._getExtractors(extractors);
        passport_1.default.use(strategyname, new passport_jwt_1.Strategy(this._opts[strategyname], function (jwt_payload, done) {
            getUser(jwt_payload.sub)
                .then(function (usr) { return done(null, usr); })
                .catch(function (err) {
                console.error("\u001B[30m[Smaug]\u001B[0m \u001B[31m Unexpected error\u001B[0m: ", err);
                return done(err);
            });
        }));
    };
    /**
     * generate jwt hash (jwt.io). Expires uses ms syntax (https://github.com/rauchg/ms.js)
     * @param {object} data
     * @param {string} expires
     * @returns {string} token
     */
    Authenticator.prototype.getToken = function (data, expires, strategy) {
        if (strategy === void 0) { strategy = "jwt"; }
        return jsonwebtoken_1.default.sign(data, this._opts[strategy].secretOrKey, {
            expiresIn: expires,
        });
    };
    /**
     * Check user login and permissions
     * @param {array} roles
     * @returns {array of functions}
     */
    Authenticator.prototype.authenticate = function (roles, strategy) {
        if (strategy === void 0) { strategy = "jwt"; }
        var config = { session: false };
        if (!roles || roles == "*") {
            return [passport_1.default.authenticate(strategy, config)];
        }
        return [passport_1.default.authenticate(strategy, config), (0, permission_1.default)(roles)];
    };
    /**
     * Check user login and permissions with user rules
     */
    Authenticator.prototype.authorize = function (req, res, next, model, method, custom, strategy) {
        if (strategy === void 0) { strategy = "jwt"; }
        // Check if it authorized
        passport_1.default.authenticate(strategy, {
            session: false,
        })(req, res, function () {
            if (!(0, validators_1.isAuthenticatedRequest)(req)) {
                return res.status(403).send("notAuthorized");
            }
            // Check if the user has the rules to access this method
            if ((0, validators_1.withModelAndMethod)(req, model, method)) {
                // The user has full access to this method
                if (req.user.Rules[model][method] === true) {
                    return next();
                }
                // Check if the user is validating with a custom method
                if (typeof req.user.Rules[model][method] === "string" &&
                    req.user.Rules[model][method] === "custom" &&
                    (0, validators_1.isCustomValidator)(custom)) {
                    return validate(custom(req), res, next);
                }
                // Check if the user needs to be owner of the resource
                if (typeof req.user.Rules[model][method] === "string" &&
                    req.user.Rules[model][method] === "self" &&
                    (0, validators_1.isSelfObject)(custom)) {
                    return validate((0, ownership_1.validateOwnership)(req, custom), res, next);
                }
            }
            // The user is not authorized
            return res.status(403).send("notAuthorized");
        });
    };
    return Authenticator;
}());
exports.default = new Authenticator();
function validate(promise, res, next) {
    return __awaiter(this, void 0, void 0, function () {
        var success, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, promise];
                case 1:
                    success = _a.sent();
                    if (success) {
                        return [2 /*return*/, next()];
                    }
                    throw new Error("validation failed");
                case 2:
                    error_1 = _a.sent();
                    console.error("\u001B[30m[Smaug]\u001B[0m \u001B[31m Unexpected error\u001B[0m: ", error_1);
                    return [2 /*return*/, res.status(403).send("notAuthorized")];
                case 3: return [2 /*return*/];
            }
        });
    });
}
//# sourceMappingURL=Authenticator.js.map