"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isWithAuthenticationUserProps = void 0;
function isWithAuthenticationUserProps(props) {
    return props.hasOwnProperty("authentication");
}
exports.isWithAuthenticationUserProps = isWithAuthenticationUserProps;
//# sourceMappingURL=validators.js.map