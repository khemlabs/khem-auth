"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UNSAFE_setAuthorizationHeaderFromQueryToken = exports.withAuthenticatedUser = exports.withAuthenticatedUsername = exports.withAuthenticatedUserId = exports.withRulesAuthorization = exports.withAuthorization = exports.withAuthentication = void 0;
var Authenticator_1 = __importDefault(require("../Authenticator"));
var helpers_1 = require("./helpers");
var validators_1 = require("./validators");
/**
 * Adds an express middleware that will validate if there is a logged user
 *
 * @param middleware [optional] The endpoint method that will be called if all middleware succeeded
 */
function withAuthentication(middleware) {
    var auth = Authenticator_1.default.authenticate("*");
    return (0, helpers_1.getMiddlewares)(auth, middleware);
}
exports.withAuthentication = withAuthentication;
/**
 * Adds an express middleware that will validate if the logged user type is listed in the received roles
 *
 * @param roles The roles that will use the khem-auth library
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
function withAuthorization(roles, middleware) {
    var auth = Authenticator_1.default.authenticate(roles);
    return (0, helpers_1.getMiddlewares)(auth, middleware);
}
exports.withAuthorization = withAuthorization;
/**
 * Adds an express middleware that will validate if the logged user complies with the rules saved in the user object
 *
 * @param model The name of the model for the authorization role
 * @param method the name of the method that will access "list" | "read" | "create" | "update" | "delete"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
function withRulesAuthorization(model, method, middleware, custom) {
    var auth = function (req, res, next) {
        return Authenticator_1.default.authorize(req, res, next, model, method, custom);
    };
    return (0, helpers_1.getMiddlewares)(auth, middleware);
}
exports.withRulesAuthorization = withRulesAuthorization;
/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "id" is the same as the logged user
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of authentication middlewares (khem-auth) and use them to validate the request
 *
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user type is needed
 *
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
function withAuthenticatedUserId(props) {
    return withAuthenticatedUser(__assign({ paramKey: "id" }, props));
}
exports.withAuthenticatedUserId = withAuthenticatedUserId;
/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "username" is the same as the logged user
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of authentication middlewares (khem-auth) and use them to validate the request
 *
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user types is needed
 *
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
function withAuthenticatedUsername(props) {
    return withAuthenticatedUser(__assign({ paramKey: "username" }, props));
}
exports.withAuthenticatedUsername = withAuthenticatedUsername;
/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "paramKey" is the same as the logged user _id
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of khem-auth authentication middlewares and use them to validate the request
 *
 * @param paramKey The parameter key used to validate the logged user
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user types is needed
 *
 * @param paramKey The parameter key used to validate the logged user
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
function withAuthenticatedUser(props) {
    if ((0, validators_1.isWithAuthenticationUserProps)(props)) {
        return (0, helpers_1.getMiddlewares)((0, helpers_1.getAuth)(props.authentication), props.middleware);
    }
    return (0, helpers_1.getMiddlewares)(__spreadArray(__spreadArray([], withAuthentication(), true), [
        (0, helpers_1.getWithParameterMiddleware)(props.paramKey || "id", props.adminTypes),
    ], false), props.middleware);
}
exports.withAuthenticatedUser = withAuthenticatedUser;
/**
 * Don't use this middleware unless absolutely necessary
 * -----------------------------------------------------
 *
 * Passing a JWT in the URL query is a bad practice as defined in the protocol
 *
 * @see https://tools.ietf.org/html/rfc6750
 */
function UNSAFE_setAuthorizationHeaderFromQueryToken() {
    return function (req, _, next) {
        // @ts-ignore
        req.headers.authorization = "Bearer ".concat(req.query.token);
        next();
    };
}
exports.UNSAFE_setAuthorizationHeaderFromQueryToken = UNSAFE_setAuthorizationHeaderFromQueryToken;
//# sourceMappingURL=index.js.map