import express, { RequestHandler } from "express";
import { AuthMiddleware, AuthenticatedUserBy, CustomValidator, RuleKey, Self, WithAuthenticatedKey, WithAuthenticatedUserProps, WithID, WithUsername } from "../../types";
/**
 * Adds an express middleware that will validate if there is a logged user
 *
 * @param middleware [optional] The endpoint method that will be called if all middleware succeeded
 */
export declare function withAuthentication<MiddlewareType extends AuthMiddleware = AuthMiddleware>(middleware?: MiddlewareType | MiddlewareType[]): RequestHandler[];
/**
 * Adds an express middleware that will validate if the logged user type is listed in the received roles
 *
 * @param roles The roles that will use the khem-auth library
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export declare function withAuthorization<MiddlewareType extends AuthMiddleware = AuthMiddleware>(roles: string | string[], middleware?: MiddlewareType | MiddlewareType[]): RequestHandler[];
/**
 * Adds an express middleware that will validate if the logged user complies with the rules saved in the user object
 *
 * @param model The name of the model for the authorization role
 * @param method the name of the method that will access "list" | "read" | "create" | "update" | "delete"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export declare function withRulesAuthorization<MiddlewareType extends AuthMiddleware = AuthMiddleware>(model: string, method: RuleKey, middleware?: MiddlewareType | MiddlewareType[], custom?: CustomValidator | Self): RequestHandler[];
/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "id" is the same as the logged user
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of authentication middlewares (khem-auth) and use them to validate the request
 *
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user type is needed
 *
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export declare function withAuthenticatedUserId(props: WithAuthenticatedKey<WithID>): RequestHandler[];
/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "username" is the same as the logged user
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of authentication middlewares (khem-auth) and use them to validate the request
 *
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user types is needed
 *
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export declare function withAuthenticatedUsername(props: WithAuthenticatedKey<WithUsername>): RequestHandler[];
/**
 * Adds an express middleware that will validate if the logged user is "admin" or if the requested "paramKey" is the same as the logged user _id
 *
 * With authentication middleware
 * ===============================
 *
 * It receives an authentication or array of khem-auth authentication middlewares and use them to validate the request
 *
 * @param paramKey The parameter key used to validate the logged user
 * @param authentication One or more authentication middlewares (example: withAuthentication()), if not received, a custom validation method will be used
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 *
 * Without authentication middleware
 * ==================================
 *
 * It uses a custom validation method to validate the request, in this case an array with the "admin" user types is needed
 *
 * @param paramKey The parameter key used to validate the logged user
 * @param adminTypes Array with the user types considered "admin"
 * @param middleware [optional] The endpoint method that will be called if all middlewares succeeded
 */
export declare function withAuthenticatedUser<ParamKey extends string, MiddlewareType extends AuthenticatedUserBy<ParamKey>>(props: WithAuthenticatedUserProps<ParamKey, MiddlewareType>): RequestHandler[];
/**
 * Don't use this middleware unless absolutely necessary
 * -----------------------------------------------------
 *
 * Passing a JWT in the URL query is a bad practice as defined in the protocol
 *
 * @see https://tools.ietf.org/html/rfc6750
 */
export declare function UNSAFE_setAuthorizationHeaderFromQueryToken(): (req: express.Request, _: express.Response, next: express.NextFunction) => void;
