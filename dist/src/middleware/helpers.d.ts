import express, { RequestHandler } from "express";
import { AuthenticatedRequest, RequestWithId, ValidUsernameRequest } from "../../types";
/**
 * Creates and return the middleware that will validate the received parameter
 *
 * @param param The parameter key received in the request
 */
export declare function getWithParameterMiddleware(param: string, adminTypes?: string[]): (req: any, _res: express.Response, next: express.NextFunction) => void;
export declare function validUserKeyInRequest(param: string, req: AuthenticatedRequest<any>): req is ValidUsernameRequest;
export declare function getMiddlewares(authMiddlewares: RequestHandler | RequestHandler[], middleware?: any): any[];
export declare function getAuth(authentication: RequestHandler | RequestHandler[]): RequestHandler[];
export declare function isRequestWithId(req: any): req is AuthenticatedRequest<RequestWithId>;
