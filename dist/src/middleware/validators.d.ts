import { AuthenticatedUserBy, WithAuthenticatedUserProps, WithAuthenticationUserProps } from "../../types";
export declare function isWithAuthenticationUserProps<ParamKey extends string, MiddlewareType extends AuthenticatedUserBy<ParamKey>>(props: WithAuthenticatedUserProps<ParamKey, MiddlewareType>): props is WithAuthenticationUserProps<ParamKey, MiddlewareType>;
