"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isRequestWithId = exports.getAuth = exports.getMiddlewares = exports.validUserKeyInRequest = exports.getWithParameterMiddleware = void 0;
var error_1 = require("@khemlabs/error");
/**
 * Creates and return the middleware that will validate the received parameter
 *
 * @param param The parameter key received in the request
 */
function getWithParameterMiddleware(param, adminTypes) {
    if (adminTypes === void 0) { adminTypes = []; }
    var validateRequest = function (req, _res, next) {
        if (!req.user) {
            return next(new error_1.HttpError(error_1.HttpErrorCode.UNAUTHORIZED, "user is not logged"));
        }
        if (adminTypes.length > 0 && adminTypes.includes(req.user.type)) {
            return next();
        }
        // Invalid parameters
        if (!validUserKeyInRequest(param, req)) {
            return next(new error_1.HttpError(error_1.HttpErrorCode.BAD_REQUEST, "invalid parameters"));
        }
        var params = req.params, user = req.user;
        // Parameter not found or differs from logged user
        if (!user[param] ||
            params[param] !== user[param]) {
            return next(new error_1.HttpError(error_1.HttpErrorCode.UNAUTHORIZED, "user ".concat(req.user.username, " can't access (").concat(req.params.username || "", ")")));
        }
        // Continue
        return next();
    };
    return validateRequest;
}
exports.getWithParameterMiddleware = getWithParameterMiddleware;
function validUserKeyInRequest(param, req) {
    return req.params && typeof req.params[param] === "string";
}
exports.validUserKeyInRequest = validUserKeyInRequest;
function getMiddlewares(authMiddlewares, middleware) {
    return middleware
        ? __spreadArray(__spreadArray([], getAuth(authMiddlewares), true), getAuth(middleware), true) : __spreadArray([], getAuth(authMiddlewares), true);
}
exports.getMiddlewares = getMiddlewares;
function getAuth(authentication) {
    return Array.isArray(authentication) ? authentication : [authentication];
}
exports.getAuth = getAuth;
function isRequestWithId(req) {
    return !!req && !!req.params && !!req.params.id;
}
exports.isRequestWithId = isRequestWithId;
//# sourceMappingURL=helpers.js.map