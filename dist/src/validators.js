"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isAuthenticatedRequest = exports.isCustomValidator = exports.isSelfObject = exports.withModelAndMethod = void 0;
/**
 * Checks that the model is present in the user object and that the method key is present in the model key
 */
function withModelAndMethod(req, model, method) {
    return (req.user &&
        req.user.Rules &&
        !!req.user.Rules[model] &&
        !!req.user.Rules[model][method]);
}
exports.withModelAndMethod = withModelAndMethod;
/**
 * Validates if the received object has a valid self object
 */
function isSelfObject(self) {
    return (!!self &&
        typeof self === "object" &&
        typeof self.param === "string" &&
        (!self.mongo ||
            (typeof self.mongo === "object" &&
                typeof self.mongo.modelKey === "string" &&
                typeof self.mongo.model === "object" &&
                typeof self.mongo.model.findOne === "function")));
}
exports.isSelfObject = isSelfObject;
function isCustomValidator(validator) {
    return !!validator && typeof validator === "function";
}
exports.isCustomValidator = isCustomValidator;
function isAuthenticatedRequest(req) {
    return !!req && !!req.user && !!req.user.username;
}
exports.isAuthenticatedRequest = isAuthenticatedRequest;
//# sourceMappingURL=validators.js.map