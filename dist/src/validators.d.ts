import { Request } from "express";
import { AuthenticatedRequest, CustomValidator, RuleKey, Self } from "../types";
/**
 * Checks that the model is present in the user object and that the method key is present in the model key
 */
export declare function withModelAndMethod<MO extends string, ME extends RuleKey>(req: AuthenticatedRequest, model: MO, method: ME): boolean;
/**
 * Validates if the received object has a valid self object
 */
export declare function isSelfObject(self: any): self is Self;
export declare function isCustomValidator(validator: any): validator is CustomValidator;
export declare function isAuthenticatedRequest(req: Request): req is AuthenticatedRequest;
