"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.joinParameters = exports.validateDuplicate = void 0;
/*
If parameters are received by URL, the developer may decide to change
the name of the parameter received. Therefore, the case can be given
where the same property is received with different values.

For example:
------------
Endpoint: https://domain/endpoint/:id
https://domain/endpoint/asd WITH BODY: {_id: 'qwe'}

If self = {param: 'id, db: '_id'} hasDifferentKeys will return false
*/
function validateDuplicate(request, self, requestData) {
    var _a;
    if (requestData === void 0) { requestData = {}; }
    var paramKey = self.param;
    var dbKey = (_a = self.db) === null || _a === void 0 ? void 0 : _a.modelKey;
    var param = paramKey
        ? request.params[paramKey]
        : false;
    var data = {
        db: dbKey ? requestData[dbKey] || false : false,
        param: paramKey ? requestData[paramKey] || false : false,
    };
    if (param &&
        ((data.db && data.db !== param) || (data.param && data.param !== param))) {
        return false;
    }
    return true;
}
exports.validateDuplicate = validateDuplicate;
/**
 * Checks if the same property is present in different objects (as "body" or "query") and joins them with "params" in an unique structure
 */
function joinParameters(req) {
    // Get the method where the params are
    var request = {};
    // Get parameters
    switch (req.method) {
        case "POST":
        case "PATCH":
            request = req.body;
            break;
        case "GET":
            request = req.query;
            if (request.query) {
                var query = {};
                if (typeof request.query == "string") {
                    try {
                        query = JSON.parse(request.query);
                    }
                    catch (e) {
                        query = {};
                    }
                }
                // request keys will be overridden by query keys
                Object.assign(request, query);
            }
            break;
    }
    // Method keys will be overridden by params keys
    Object.assign(request, req.params || {});
    return request;
}
exports.joinParameters = joinParameters;
//# sourceMappingURL=helpers.js.map