/// <reference types="qs" />
import { JwtFromRequestFunction } from "passport-jwt";
import * as core from "express-serve-static-core";
import { Request, RequestHandler, Response, NextFunction } from "express";
import { BaseUser, Configuration, CustomValidator, RuleKey, Rules, Self } from "../types";
declare class Authenticator<User extends BaseUser> {
    _opts: {
        [key in string]: any;
    };
    _getExtractors(extractors?: string | JwtFromRequestFunction[]): JwtFromRequestFunction;
    /**
     * Configure passport strategy
     *
     * @param {string} secret the jwt secret string
     * @param {function} authenticate a method that the received the token sub and must return the user if its valid
     * @param {string} strategyname [optional] [default: jwt]
     * @param {string|array|null} extractors [optional] jwt extractors, can be string [only-header, url-header, only-url] or array with ExtractJwt instances [default: only-header]
     */
    configure(secret: string, authenticate: (sub: any) => Promise<User>, strategyname?: string, extractors?: string | JwtFromRequestFunction[]): import("express").Handler;
    /**
     * Configure passport strategies
     *
     * @see configure for more info
     */
    configureFromArray(configurations: Configuration<User>[]): import("express").Handler;
    /**
     * Validates that a jwt is received and uses the authenticate method to validate the logged user
     */
    _configure(secret: string, getUser: (sub: any) => Promise<User>, strategyname?: string, extractors?: string | JwtFromRequestFunction[]): void;
    /**
     * generate jwt hash (jwt.io). Expires uses ms syntax (https://github.com/rauchg/ms.js)
     * @param {object} data
     * @param {string} expires
     * @returns {string} token
     */
    getToken<T extends {
        [key in string]: any;
    }>(data: T, expires: string, strategy?: string): string;
    /**
     * Check user login and permissions
     * @param {array} roles
     * @returns {array of functions}
     */
    authenticate<P = core.ParamsDictionary, ResBody = any, ReqBody = any, ReqQuery = qs.ParsedQs>(roles?: string | string[], strategy?: string): (RequestHandler<P, ResBody, ReqBody, ReqQuery> & {
        user: User;
    })[];
    /**
     * Check user login and permissions with user rules
     */
    authorize<REQ extends Request, RES extends Response>(req: REQ, res: RES, next: NextFunction, model: keyof Rules, method: RuleKey, custom?: CustomValidator | Self, strategy?: string): void;
}
declare const _default: Authenticator<BaseUser>;
export default _default;
