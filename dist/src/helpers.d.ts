import { Request } from "express";
import { AuthenticatedRequest, Self } from "../types";
export declare function validateDuplicate(request: AuthenticatedRequest, self: Self, requestData?: any): boolean;
/**
 * Checks if the same property is present in different objects (as "body" or "query") and joins them with "params" in an unique structure
 */
export declare function joinParameters(req: Request): {
    [x: string]: any;
};
