import { AuthenticatedRequest, Self } from "../types";
/**
 * This method validates that the key received is the id of the logged user and if a mongoose model
 * is received that the resource is owned by the received id
 */
export declare function validateOwnership(request: AuthenticatedRequest, self: Self): Promise<boolean>;
