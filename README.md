# README

Khemlabs - Kickstarter - Auth

## What is this repository for?

This lib is intended for developers that are using the khemlabs kickstarter framework,
and want to authenticate users, and validate their the requests.

## CONFIGURE

```typescript
import kAuth from "khem-auth";

const SECRET_PHRASE = "MySecretPhrase";

/**
 * getUser method
 *
 * FOR MORE INFORMATION ABOUT THIS METHOD SEE THE GET_USER DOCUMENTATION BELOW
 */
async function getUser(sub: any) {
  if (typeof sub === "object" && typeof sub._id === "string") {
    const user = await UserModel.findOne({ _id: sub._id });
    return !!user;
  }
  return false;
}

// You can do this
app.use(kAuth.configure(SECRET_PHRASE, getUser));

// Or this
const authConfiguration = [
  {
    secret: SECRET_PHRASE,
    authenticate,
  },
  {
    secret: SECRET_PHRASE + "auth-email-routes",
    // This method must exist
    authenticateEmail,
    strategyname: "jwt-email",
    extractors: "only-url",
  },
  {
    secret: SECRET_PHRASE + "auth-password-routes",
    authenticate,
    strategyname: "jwt-password",
    extractors: "only-url",
  },
];
app.use(kAuth.configureFromArray(authConfiguration));
```

### GET_USER METHOD

This lib calls a method _getUser_ received on _kAuth.configure_,
this method is called with the _object obtained from the JWT hash_
and must return a valid user object with a valid Rules object.

```typescript
UserModel.getUser(jwt_payload.sub);
```

#### Rules Object

The valid format of the Rules object is as follows:

```typescript
// This object must be present in the user object returned by the "authenticate" method
type user = {
  Rules: {
    // model: Must be a valid model name
    [model]?: {
      // method: Must be 'list', 'read', 'create', 'update' or 'delete'
      [method]?:
        | boolean // true: if user can perform the action - false or UNDEFINED: if the user can't perform the action
        | "self" // if the user can only update things that owns
        | "custom"; // if a custom validation method must be used
    };
  };
};
```

##### Self validator

If the rule received is a string 'self', a valid 'self' object must be passed when calling the authorize method:

```typescript

import kAuth from 'khem-auth';

type Self = {
    param: string;
    db?: {
      modelKey: string;
      model: {
        findOne: (search: { [key in string]: string }) => Promise<
          object | null | undefined
        >;
      };
    };
  };

 // Will validate if the variable 'id' is present on the request
 // (params, body or query), if 'id' is present on the request
 // and if it matches the logged user id will authorize the request.
 const self: Self = {
   param: "id";
  }

 // Will validate if the variable 'user' is present on the request
 // (params, body or query), if 'user' is present on the request
 // and if it matches the logged user id, and if the
 // MongooseModel.findOne({"owner": request.user}) returns a valid value,
 // will authorize the request.
 const self: Self = {
  param: 'user',
  db: {
    modelKey: 'owner',
    model: MongooseModel // Can be any database, the important thing here is that the model must have a method called findOne and must return a promise with an object if successful
  }

  // Call authorize with the self object
  kAuth.authorize(req, res, next, "MyModel", "list", self);
```

##### Custom validator

If the rule received is a string 'custom', a valid 'custom' object must be passed when calling the authorize method:

```typescript
import kAuth from "khem-auth";

async function validator(req: AuthenticatedRequest) {
  const found = await MyMongooseModel.findOne({
    something: req.params.something,
  });
  return !!found;
}

// Call authorize with the validator function
kAuth.authorize(req, res, next, "MyModel", "list", validator);
```

## AUTHENTICATE

This method validates the request using the passport.authenticate or the passport library

```typescript
router.get("/", kAuth.authenticate("*"), myHandler);
```

## AUTHORIZE

This method validates that the logged user has the authorization required by the endpoint

```typescript
router.get("/", function (req, res, next) {
  kAuth.authorize(req, res, next, "MyModel", "list");
});
```

## EXPRESS MIDDLEWARE

The library offers express middleware that simplifies the library usage:

### withAuthentication

Checks that the user is logged

```typescript
import { withAuthentication } from "khem-auth/middleware";

router.get(
  "/",
  withAuthentication(function (req, _res, next) {
    // The req.user has the logged user
    console.debug(req.user);
    next();
  })
);
```

### withAuthorization

Checks that the user is logged and has the required role

```typescript
import { withAuthorization } from "khem-auth/middleware";

router.get(
  "/",
  withAuthorization(["admin"], function (req, _res, next) {
    // The req.user has the logged user
    console.debug(req.user);
    next();
  })
);
```

### withRulesAuthorization

Checks that the user is logged and has the required permissions

```typescript
import { withRulesAuthorization } from "khem-auth/middleware";

router.get(
  "/",
  withRulesAuthorization("MyModel", "list", function (req, _res, next) {
    // The req.user has the logged user
    console.debug(req.user);
    next();
  })
);
```

### withAuthenticatedUserId

Checks that the user is logged and that the id received is from the logged user, this method is a special case of "withAuthenticatedUser"

```typescript
import { withAuthenticatedUserId } from "khem-auth/middleware";

// Using admin roles
router.get(
  // The parameter :id must exists
  "/:id",
  withAuthenticatedUserId({
    // Will validate only if the user role is "admin"
    adminTypes: ["admin"],
    middleware: async function (req, res, next) {
      // The req.user has the logged user
      console.debug(req.user);
      next();
    },
  })
);

// Using a an authentication method
router.get(
  // The parameter :id must exists
  "/:id",
  withAuthenticatedUserId({
    // Wil use this validation
    authentication: withRulesAuthorization("MyModel", "list"),
    middleware: async function (req, res, next) {
      // The req.user has the logged user
      console.debug(req.user);
      next();
    },
  })
);
```

### withAuthenticatedUsername

Checks that the user is logged and that the username received is from the logged user, this method is a special case of "withAuthenticatedUser"

```typescript
import { withAuthenticatedUsername } from "khem-auth/middleware";

// Using admin roles
router.get(
  // The parameter :username must exists
  "/:username",
  withAuthenticatedUsername({
    // Will validate only if the user role is "admin"
    adminTypes: ["admin"],
    middleware: async function (req, res, next) {
      // The req.user has the logged user
      console.debug(req.user);
      next();
    },
  })
);

// Using a an authentication method
router.get(
  // The parameter :username must exists
  "/:username",
  withAuthenticatedUsername({
    // Wil use this validation
    authentication: withRulesAuthorization("MyModel", "list"),
    middleware: async function (req, res, next) {
      // The req.user has the logged user
      console.debug(req.user);
      next();
    },
  })
);
```

### withAuthenticatedUser

Checks that the user is logged and that the user "paramKey" received is from the logged user

```typescript
import { withAuthenticatedUser } from "khem-auth/middleware";

// Using admin roles
router.get(
  // The parameter :myKey must exists
  "/:myKey",
  withAuthenticatedUser({
    paramKey: "myKey",
    // Will validate only if the user role is "admin"
    adminTypes: ["admin"],
    middleware: async function (req, res, next) {
      // The req.user has the logged user
      console.debug(req.user);
      next();
    },
  })
);

// Using a an authentication method
router.get(
  // The parameter :myKey must exists
  "/:myKey",
  withAuthenticatedUser({
    paramKey: "myKey",
    // Wil use this validation
    authentication: withRulesAuthorization("MyModel", "list"),
    middleware: async function (req, res, next) {
      // The req.user has the logged user
      console.debug(req.user);
      next();
    },
  })
);
```

## Who do I talk to?

- dnangelus repo owner and admin
- developer elgambet and khemlabs
