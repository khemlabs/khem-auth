import httpMocks from "node-mocks-http";
import chai from "chai";

export function getRequest(token: string, params?: { [key in string]: any }) {
  const request = httpMocks.createRequest({
    headers: {
      authorization: `Bearer ${token}`,
    },
    params,
  });

  return request;
}

export function getUnauthorizedResponse(done: Mocha.Done, expectCode?: number) {
  let doneCalled = false;
  // @TODO: NOT WORKING
  // const response = httpMocks.createResponse();
  // response.on("finish", () => {
  //   try {
  //     console.debug(response.statusCode);
  //     if (expectCode) {
  //       chai.expect(response.statusCode).to.equal(expectCode);
  //     }
  //     done();
  //   } catch (error) {
  //     done(error);
  //   }
  // });
  // response.on("error", (error) => {
  //   console.debug("error: ", error);
  //   done(error);
  // });

  const response: any = {
    status: (code: number) => {
      try {
        chai
          .expect(expectCode, `unexpected code (${code}) was received`)
          .to.be.a("number");
        chai.expect(expectCode).to.equal(code);
        if (!doneCalled) {
          doneCalled = true;
          done();
        }
      } catch (error) {
        if (!doneCalled) {
          doneCalled = true;
          done(error);
        }
      }
      return {
        send: () => {
          try {
            chai
              .expect(expectCode, `unexpected code (${code}) was received`)
              .to.be.a("number");
            chai.expect(expectCode).to.equal(code);
            if (!doneCalled) {
              doneCalled = true;
              done();
            }
          } catch (error) {
            if (!doneCalled) {
              doneCalled = true;
              done(error);
            }
          }
        },
      };
    },
    end(data: string) {
      try {
        chai
          .expect(expectCode, `unexpected response (${data}) was received`)
          .to.be.a("number");
        chai.expect(expectCode).to.equal(401);
        chai.expect(data).to.equal("Unauthorized");
        if (!doneCalled) {
          doneCalled = true;
          done();
        }
      } catch (error) {
        if (!doneCalled) {
          doneCalled = true;
          done(error);
        }
      }
    },
    send(data: string) {
      try {
        chai
          .expect(expectCode, `unexpected response (${data}) was received`)
          .to.be.a("number");
        chai.expect(expectCode).to.equal(401);
        chai.expect(data).to.equal("Unauthorized");
        if (!doneCalled) {
          doneCalled = true;
          done();
        }
      } catch (error) {
        if (!doneCalled) {
          doneCalled = true;
          done(error);
        }
      }
    },
  };

  return response;
}
