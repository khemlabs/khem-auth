const tokens = {
  adminToken: "",

  updateToken: "",

  listToken: "",

  selfToken: "",

  customToken: "",
};

export function getToken(key: keyof typeof tokens) {
  return tokens[key];
}

export function setToken(key: keyof typeof tokens, value: string) {
  tokens[key] = value;
}
