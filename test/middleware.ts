import e from "express";

let middleware: e.Handler | null = null;

export function setMiddleWare(method: e.Handler) {
  middleware = method;
}

export function getMiddleware() {
  return middleware;
}
