import "mocha";

import chai from "chai";

import kAuth from "../src/Authenticator";

import { getToken } from "./tokens";
import { getMiddleware } from "./middleware";
import { getRequest, getUnauthorizedResponse } from "./generators";

describe("Validate authentications: ", function () {
  it("it expects to authorize request (admin user) ", function (done) {
    authenticate(done, getToken("adminToken"));
  });

  it("it expects to authorize request (update user) ", function (done) {
    authenticate(done, getToken("updateToken"));
  });

  it("it expects to authorize request (list user) ", function (done) {
    authenticate(done, getToken("listToken"));
  });

  it("it expects to authorize request (self user) ", function (done) {
    authenticate(done, getToken("selfToken"));
  });

  it("it expects to authorize request (custom user) ", function (done) {
    authenticate(done, getToken("customToken"));
  });

  it("it expects to unauthorize request (without user) ", function (done) {
    authenticate(done, "JsdafkdnjsvINVALIDTOKEN", 401);
  });
});

function authenticate(done: Mocha.Done, token: string, expectCode?: number) {
  const middleware = getMiddleware();
  chai.expect(middleware).to.be.a("function");
  chai.expect(token).to.be.a("string");
  chai.expect(token.length).to.be.greaterThan(0);
  const request = getRequest(token);
  const response = getUnauthorizedResponse(done, expectCode);
  middleware!(request, response, () => {
    const all = kAuth.authenticate("*");
    all.every((auth, index) =>
      auth(request, response, () => {
        if (index === all.length - 1) {
          chai.expect(request.user).to.be.an("object");
          chai.expect((request.user as any)._id).to.be.a("string");
          done();
        }
      })
    );
  });
}
