import "mocha";

import chai from "chai";

import kAuth from "../src/Authenticator";

import {
  adminUser,
  customUser,
  listUser,
  selfUser,
  updateUser,
  users,
  secret,
} from "./data";
import { getToken, setToken } from "./tokens";
import { getMiddleware, setMiddleWare } from "./middleware";

describe("Validate token generation: ", function () {
  it("kAuth should be configured", function (done) {
    setMiddleWare(kAuth.configure(secret, searchUser));
    const middleware = getMiddleware();
    chai.expect(middleware).to.be.a("function");
    done();
  });

  it("it expects to generate an admin token", function (done) {
    setToken("adminToken", kAuth.getToken({ sub: adminUser }, "10m"));
    const adminToken = getToken("adminToken");
    chai.expect(adminToken).to.be.a("string");
    chai.expect(adminToken.length).to.be.greaterThan(0);
    done();
  });

  it("it expects to generate an update token", function (done) {
    setToken("updateToken", kAuth.getToken({ sub: updateUser }, "10m"));
    const updateToken = getToken("updateToken");
    chai.expect(updateToken).to.be.a("string");
    chai.expect(updateToken.length).to.be.greaterThan(0);
    done();
  });

  it("it expects to generate a list token", function (done) {
    setToken("listToken", kAuth.getToken({ sub: listUser }, "10m"));
    const listToken = getToken("listToken");
    chai.expect(listToken).to.be.a("string");
    chai.expect(listToken.length).to.be.greaterThan(0);
    done();
  });

  it("it expects to generate a self token", function (done) {
    setToken("selfToken", kAuth.getToken({ sub: selfUser }, "10m"));
    const selfToken = getToken("selfToken");
    chai.expect(selfToken).to.be.a("string");
    chai.expect(selfToken.length).to.be.greaterThan(0);
    done();
  });

  it("it expects to generate a custom token", function (done) {
    setToken("customToken", kAuth.getToken({ sub: customUser }, "10m"));
    const customToken = getToken("customToken");
    chai.expect(customToken).to.be.a("string");
    chai.expect(customToken.length).to.be.greaterThan(0);
    done();
  });
});

async function searchUser(data: any) {
  const found = !!data._id && users.find((user) => user._id === data._id);
  if (!found) {
    throw new Error(`user not found`);
  }
  return found;
}
