import "mocha";

import chai from "chai";

import kAuth from "../src/Authenticator";

import { getToken } from "./tokens";
import { getMiddleware } from "./middleware";
import { NextFunction } from "express";
import { customUser, selfUser } from "./data";
import { getRequest, getUnauthorizedResponse } from "./generators";
import { AuthenticatedRequest, CustomValidator, RuleKey, Self } from "../types";

describe("Validate authorizations: ", function () {
  it("it expects to authorize create request (admin user) ", function (done) {
    authorize(done, getToken("adminToken"), "private", "create");
  });

  it("it expects to unauthorize create request (list user) ", function (done) {
    authorize(
      done,
      getToken("listToken"),
      "private",
      "create",
      undefined,
      undefined,
      403
    );
  });

  it("it expects to authorize update request (update user) ", function (done) {
    authorize(done, getToken("updateToken"), "private", "update");
  });

  it("it expects to unauthorize update request (self user) ", function (done) {
    authorize(
      done,
      getToken("listToken"),
      "private",
      "update",
      undefined,
      undefined,
      403
    );
  });

  it("it expects to authorize list request (list user) ", function (done) {
    authorize(done, getToken("listToken"), "private", "list");
  });

  it("it expects to unauthorize list request (self user) ", function (done) {
    authorize(
      done,
      getToken("selfToken"),
      "private",
      "list",
      undefined,
      undefined,
      403
    );
  });

  it("it expects to authorize list request (self user) ", function (done) {
    authorize(
      done,
      getToken("selfToken"),
      "private",
      "list",
      {
        param: "user",
      },
      { user: selfUser._id }
    );
  });

  it("it expects to authorize update request (self user with db) ", function (done) {
    authorize(
      done,
      getToken("selfToken"),
      "private",
      "list",
      {
        param: "user",
        db: {
          model: {
            findOne: async (where) => {
              if (where.customKey === selfUser._id) {
                return selfUser;
              }
              return null;
            },
          },
          modelKey: "customKey",
        },
      },
      { user: selfUser._id }
    );
  });

  it("it expects to authorize list request (custom user) ", function (done) {
    authorize(
      done,
      getToken("customToken"),
      "private",
      "list",
      async (req: any) => {
        return req.user.username === customUser.username;
      }
    );
  });
});

function authorize(
  done: Mocha.Done,
  token: string,
  model: string,
  method: RuleKey,
  custom?: CustomValidator | Self,
  params?: { [key in string]: string },
  expectCode?: number
) {
  const middleware = getMiddleware();
  chai.expect(middleware).to.be.a("function");
  chai.expect(token).to.be.a("string");
  chai.expect(token.length).to.be.greaterThan(0);
  const request = getRequest(token, params);
  const response = getUnauthorizedResponse(done, expectCode);
  middleware!(request, response, () => {
    const next = ((_req: AuthenticatedRequest) => {
      done();
    }) as NextFunction;
    kAuth.authorize(request, response, next, model, method, custom);
  });
}
