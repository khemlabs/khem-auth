import { BaseUser, UserValidation } from "../types";

export const secret = "123";

export const full: UserValidation = {
  list: true,
  read: true,
  update: true,
  create: true,
  delete: true,
};

export const update: UserValidation = {
  list: true,
  read: true,
  update: true,
  create: true,
  delete: false,
};

export const list: UserValidation = {
  list: true,
  read: true,
  update: false,
  create: false,
  delete: false,
};

export const self: UserValidation = {
  list: "self",
  read: "self",
  update: false,
  create: false,
  delete: false,
};

export const custom: UserValidation = {
  list: "custom",
  read: "custom",
  update: false,
  create: false,
  delete: false,
};

export const adminUser: BaseUser = {
  _id: "1",
  username: "admin",
  type: "admin",
  Rules: {
    user: full,
    private: full,
  },
};

export const updateUser: BaseUser = {
  _id: "1",
  username: "admin",
  type: "admin",
  Rules: {
    user: update,
    private: update,
  },
};

export const listUser: BaseUser = {
  _id: "2",
  username: "list",
  type: "list",
  Rules: {
    user: list,
    private: list,
  },
};

export const selfUser: BaseUser = {
  _id: "3",
  username: "self",
  type: "self",
  Rules: {
    user: self,
    private: self,
  },
};

export const customUser: BaseUser = {
  _id: "4",
  username: "custom",
  type: "custom",
  Rules: {
    user: custom,
    private: custom,
  },
};

export const users: BaseUser[] = [
  adminUser,
  updateUser,
  listUser,
  selfUser,
  customUser,
];
